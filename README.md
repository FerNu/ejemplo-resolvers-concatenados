## Configurar Query con resolvers concatenados

1.  **Crear función lambda. (Igual que punto anterior)**

-   Elegir Author from Scratch

-   Escribir un nombre para la función

-   Runtime: Node.js 18.x

-   Arquitectura: x86_64

-   Acceder a un usuario con permisos de lambda y crear access keys.

2.  **Configurar el repositorio (Igual que punto anterior)**

Para Gitlab :

-   Ir a Settings -\> CI/CD-\>variables

-   Agregar variable

-   Crear variable AWS_ACCESS_KEY_ID y darle el valor de Access key

-   Crear variable AWS_SECRET_ACCESS_KEY y darle el valor de secret Access key

-   Asegurarse de que ambas variables esten secretas y enmascaradas

-   Editar el archivo **.gitlab-ci.yml**

-   Cambiar la región por la del lambda creado

-   Cambiar el nombre por el del lambda creado

-   Al realizar el commit se debería actualizar el código de la función
    lambda.

3.  **Agregar nueva query a appsync**

-   Ingresar a la API GraphQL

-   Si no existe, agregar la funcion lambda como data source.

-   Modificar schema de la api, se van a agregar en este caso un tipo nuevo (el tipo field_nombre) con los siguientes atributos.

Agregar al schema:
```
type field_nombre {
	id: Int
	crop_date: String
	crop_id: Int
	geometry: String
	geometryWKB: String
	has: Float
	nombre_de_duenio: String
}

```
-   Luego se agrega la query get_field_nombre, al tipo Query ya existente, esta funcion va a retornar un mensaje tipo field_nombre y va a tener los parametros FIELDID y lang. 
```

type Query {
    get_field_nombre(FIELDID: Int, lang: String): field_nombre
    ...
    ...
}
schema {
    query : Query
    ...
}
```
-   Ir a **Functions** y seleccionar crear una nueva funcion

-   Seleccionar la funcion lambda recien creada como data source y agregar el siguiente codigo

    ``` 
    export function request(ctx) {

    return {

    "version": "2018-05-29",

    "operation": "Invoke",

    "payload": {
      "field": ctx.prev.result}

    };

    }
    export function response(ctx) {
    return ctx.result
}

    ```

En el objeto "payload" se encuentran los valores que se envian a la funcion lambda, en este caso es el resultado de la funcion anterior, que estara concatenada con esta.

-   Volver a la pagina de schema, a la derecha, buscar la query y hacer click en el botón Attach para
    agregar un resolver a la query

-   Elegir opción Pipeline resolver como tipo y en este caso VTL como
    runtime.

-   Agregar el siguiente codigo en before mapping template

    ```
    $util.qr($ctx.stash.put("typeName", "Query"))
    $util.qr($ctx.stash.put("fieldName", "get_field_info"))
    {}
    ```
    Esto sirve para agregar variables de contexto que se va a enviar a la funcion lambda.

-   Seleccionar primero la funcion de campo360 ya creada para los resolvers (InvokeApigraphql3600c597cffLambdaDataSource) que va a buscar un field en campo360.

-   La funcion ya esta creada asi que no hacen falta configuraciones extra

-   Agregar ahora la funcion nueva, asegurarse de que esté segunda.


4.  **Probar query**

-   Ir a la opción queries en el menú.

-   Elegir la query "get_field_nombre" que se acaba de crear.

-   Agregar los parametros FIELDID y lang, FIELDID tiene que ser un id de lote valido que exista en la base, por ejemplo **20333**, los lang validos son "es", "en" y "pt", para Español, Ingles y Portugues respectivamente.

-   Hacer click en "Run", debería mostrar un objeto json con los datos propios del field y el atributo extra "nombre_de_duenio", con el valor hardcodeado "Juan Carlos" si no se modifico esta funcion.
